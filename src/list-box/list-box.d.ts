/// <reference path="../drop-down-base/drop-down-base-model.d.ts" />
import { DropDownBase, SelectEventArgs } from '../drop-down-base/drop-down-base';
import { FieldSettingsModel } from '../drop-down-base/drop-down-base-model';
import { ChildProperty, BaseEventArgs } from '@syncfusion/ej2-base';
import { ModuleDeclaration, EmitType } from '@syncfusion/ej2-base';
import { SelectionSettingsModel, ListBoxModel, ToolbarSettingsModel } from './list-box-model';
export declare type SelectionMode = 'Multiple' | 'Single';
export declare type ToolBarPosition = 'Left' | 'Right';
export declare class SelectionSettings extends ChildProperty<SelectionSettings> {
    /**
     * Specifies the selection modes. The possible values are
     * * `Single`: Allows you to select a single item in the ListBox.
     * * `Multiple`: Allows you to select more than one item in the ListBox.
     * @default 'Multiple'
     */
    mode: SelectionMode;
    /**
     * If 'showCheckbox' is set to true, then 'checkbox' will be visualized in the list item.
     * @default false
     */
    showCheckbox: boolean;
    /**
     * Allows you to either show or hide the selectAll option on the component.
     * @default false
     */
    showSelectAll: boolean;
}
export declare class ToolbarSettings extends ChildProperty<ToolbarSettings> {
    /**
     * Specifies the list of tools for dual ListBox.
     * The predefined tools are 'moveUp', 'moveDown', 'moveTo', 'moveFrom', 'moveAllTo', and 'moveAllFrom'.
     * @default []
     */
    items: string[];
    /**
     * Positions the toolbar before/after the ListBox.
     * The possible values are:
     * * Left: The toolbar will be positioned to the left of the ListBox.
     * * Right: The toolbar will be positioned to the right of the ListBox.
     * @default 'Right'
     */
    position: ToolBarPosition;
}
/**
 * The ListBox is a graphical user interface component used to display a list of items.
 * Users can select one or more items in the list using a checkbox or by keyboard selection.
 * It supports sorting, grouping, reordering, and drag and drop of items.
 * ```html
 * <select id="listbox">
 *      <option value='1'>Badminton</option>
 *      <option value='2'>Basketball</option>
 *      <option value='3'>Cricket</option>
 *      <option value='4'>Football</option>
 *      <option value='5'>Tennis</option>
 * </select>
 * ```
 * ```typescript
 * <script>
 *   var listObj = new ListBox();
 *   listObj.appendTo("#listbox");
 * </script>
 * ```
 */
export declare class ListBox extends DropDownBase {
    private prevSelIdx;
    private listCurrentOptions;
    private checkBoxSelectionModule;
    private tBListBox;
    private initLoad;
    private spinner;
    private initialSelectedOptions;
    private showSelectAll;
    private selectAllText;
    private unSelectAllText;
    private popupWrapper;
    /**
     * Sets the CSS classes to root element of this component, which helps to customize the
     * complete styles.
     * @default ''
     */
    cssClass: string;
    /**
     * Sets the specified item to the selected state or gets the selected item in the ListBox.
     * @default []
     * @aspType object
     */
    value: string[] | number[] | boolean[];
    /**
     * Sets the height of the ListBox component.
     * @default ''
     */
    height: number | string;
    /**
     * If 'allowDragAndDrop' is set to true, then you can perform drag and drop of the list item.
     * ListBox contains same 'scope' property enables drag and drop between multiple ListBox.
     * @default false
     */
    allowDragAndDrop: boolean;
    /**
     * Defines the scope value to group sets of draggable and droppable ListBox.
     * A draggable with the same scope value will be accepted by the droppable.
     * @default ''
     */
    scope: string;
    /**
     * Triggers while rendering each list item.
     * @event
     */
    beforeItemRender: EmitType<BeforeItemRenderEventArgs>;
    /**
     * Triggers while selecting the list item.
     * @event
     */
    select: EmitType<SelectEventArgs>;
    /**
     * Triggers while select / unselect the list item.
     * @event
     */
    change: EmitType<ListBoxChangeEventArgs>;
    /**
     * Triggers after dragging the list item.
     * @event
     */
    dragStart: EmitType<DragEventArgs>;
    /**
     * Triggers while dragging the list item.
     * @event
     */
    drag: EmitType<DragEventArgs>;
    /**
     * Triggers before dropping the list item on another list item.
     * @event
     */
    drop: EmitType<DragEventArgs>;
    /**
     * Triggers when data source is populated in the list.
     * @event
     * @private
     */
    dataBound: EmitType<Object>;
    /**
     * Accepts the template design and assigns it to the group headers present in the list.
     * @default null
     * @private
     */
    groupTemplate: string;
    /**
     * Accepts the template design and assigns it to list of component
     * when no data is available on the component.
     * @default 'No Records Found'
     * @private
     */
    noRecordsTemplate: string;
    /**
     * Accepts the template and assigns it to the list content of the component
     * when the data fetch request from the remote server fails.
     * @default 'The Request Failed'
     * @private
     */
    actionFailureTemplate: string;
    /**
     * specifies the z-index value of the component popup element.
     * @default 1000
     * @private
     */
    zIndex: number;
    /**
     * ignoreAccent set to true, then ignores the diacritic characters or accents when filtering.
     * @private
     */
    ignoreAccent: boolean;
    /**
     * Specifies the toolbar items and its position.
     * @default { items: [], position: 'Right' }
     */
    toolbarSettings: ToolbarSettingsModel;
    /**
     * Specifies the selection mode and its type.
     * @default { mode: 'Multiple', type: 'Default' }
     */
    selectionSettings: SelectionSettingsModel;
    /**
     * Constructor for creating the ListBox component.
     */
    constructor(options?: ListBoxModel, element?: string | HTMLElement);
    /**
     * Build and render the component
     * @private
     */
    render(): void;
    private initWrapper;
    private initDraggable;
    private initToolbar;
    private createButtons;
    protected validationAttribute(input: HTMLInputElement, hiddenSelect: HTMLSelectElement): void;
    private setHeight;
    private setCssClass;
    private setEnable;
    protected showSpinner(): void;
    protected hideSpinner(): void;
    protected onActionComplete(ulElement: HTMLElement, list: {
        [key: string]: Object;
    }[] | boolean[] | string[] | number[], e?: Object): void;
    private initToolbarAndStyles;
    private triggerDragStart;
    private triggerDrag;
    private dragEnd;
    private getComponent;
    protected listOption(dataSource: {
        [key: string]: Object;
    }[] | string[] | number[] | boolean[], fields: FieldSettingsModel): FieldSettingsModel;
    private triggerBeforeItemRender;
    requiredModules(): ModuleDeclaration[];
    /**
     * This method is used to enable or disable the items in the ListBox based on the items and enable argument.
     * @param items Text items that needs to be enabled/disabled.
     * @param enable Set `true`/`false` to enable/disable the list items.
     * @returns void
     */
    enableItems(items: string[], enable?: boolean): void;
    /**
     * Based on the state parameter, specified list item will be selected/deselected.
     * @param items Array of text value of the item.
     * @param state Set `true`/`false` to select/un select the list items.
     * @returns void
     */
    selectItems(items: string[], state?: boolean): void;
    /**
     * Based on the state parameter, entire list item will be selected/deselected.
     * @param state Set `true`/`false` to select/un select the entire list items.
     * @returns void
     */
    selectAll(state?: boolean): void;
    /**
     * Adds a new item to the list. By default, new item appends to the list as the last item,
     * but you can insert based on the index parameter.
     * @param  { Object[] } items - Specifies an array of JSON data or a JSON data.
     * @param { number } itemIndex - Specifies the index to place the newly added item in the list.
     * @return {void}.
     */
    addItems(items: {
        [key: string]: Object;
    }[] | {
        [key: string]: Object;
    }, itemIndex?: number): void;
    private selectAllItems;
    private wireEvents;
    private wireToolbarEvent;
    private unwireEvents;
    private clickHandler;
    private checkSelectAll;
    private selectHandler;
    private triggerSelectAndChange;
    private getDataByElems;
    private toolbarClickHandler;
    private moveUpDown;
    private moveTo;
    private moveFrom;
    private moveData;
    private moveAllTo;
    private moveAllFrom;
    private moveAllData;
    private changeData;
    private getSelectedItems;
    private getScopedListBox;
    private getDragArgs;
    private keyDownHandler;
    private upDownKeyHandler;
    private focusOutHandler;
    private getValidIndex;
    private updateSelectedOptions;
    private clearSelection;
    private setSelection;
    private updateSelectTag;
    private updateToolBarState;
    private isSelected;
    private getSelectTag;
    private getToolElem;
    private formResetHandler;
    /**
     * Return the module name.
     * @private
     */
    getModuleName(): string;
    /**
     * Get the properties to be maintained in the persisted state.
     */
    protected getPersistData(): string;
    protected getLocaleName(): string;
    destroy(): void;
    /**
     * Called internally if any of the property value changed.
     * @returns void
     * @private
     */
    onPropertyChanged(newProp: ListBoxModel, oldProp: ListBoxModel): void;
}
/**
 * Interface for before item render event.
 */
export interface BeforeItemRenderEventArgs extends BaseEventArgs {
    element: Element;
    item: {
        [key: string]: Object;
    };
}
/**
 * Interface for drag and drop event.
 */
export interface DragEventArgs {
    elements: Element[];
    items: Object[];
    target?: Element;
}
/**
 * Interface for select event args.
 */
export interface ListBoxSelectEventArgs extends BaseEventArgs {
    elements: Element[];
    items: Object[];
}
/**
 * Interface for change event args.
 */
export interface ListBoxChangeEventArgs extends BaseEventArgs {
    elements: Element[];
    items: Object[];
    value: number | string | boolean;
    event: Event;
}
